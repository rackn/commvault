#!/usr/bin/env bash

cpname="commvault"
version=$(tools/version.sh)

cd commvault
rm -f ._Version.meta
drpcli contents bundle ../${cpname}.json Version=$version
drpcli contents bundle ../${cpname}.yaml Version=$version --format=yaml
cd ..

mkdir -p rebar-catalog/${cpname}
cp ${cpname}.json rebar-catalog/${cpname}/$version.json

